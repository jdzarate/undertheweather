import { Box, Typography } from "@mui/material";
import CircularProgress from '@mui/material/CircularProgress';
import React from "react";

interface IProgressProps{
  text?: string
}

export default function Progress(props: IProgressProps){
  return(
    <Box sx={{ display: 'flex', height: '100vh', flexDirection:'column', justifyContent:'center', alignItems:'center' }}>
      <CircularProgress />
      <Typography>{props.text}</Typography>
    </Box>
  )
}