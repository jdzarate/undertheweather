import { Accordion, AccordionDetails, AccordionSummary, Stack, Table, TableBody, TableCell, TableContainer, TableRow, Typography, colors } from "@mui/material";
import React from "react";
import {test_data} from "../utils";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { intlFormat, parse as parseDate, format as formatDate } from "date-fns";

interface IForecastProps{
  unit: 'celcius'|'fahrenheit';
  data: any
}

export default function Forecast(props: IForecastProps){
  const data = props.data;
  return(
    <div>
      {data!==undefined?
      <div>
        <div>
					<Typography textAlign={"left"} variant="h6">{data.location.name}</Typography>
					<Typography textAlign={"left"}>{data.location.region}, {data.location.country}</Typography>
				</div>      
        <br/>
        <Stack>
          {data.forecast.forecastday.map((forecast:any, index:number)=>{
            return(
              <Accordion key={`forecast_${index}`}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                >
                  <Stack direction={"row"} width={"100%"} spacing={2} alignItems="center">
                    <div style={{flexGrow:1, textAlign:'left'}}>
                      <Typography>{formatDate(parseDate(`${forecast.date} 23:59`,'yyyy-MM-dd HH:mm',new Date()), 'E, LLL dd')}</Typography>
                      <Typography>{forecast.day.condition.text}</Typography>
                    </div>
                    <div>
                      <Typography color={colors.blue[500]}>{forecast.day.daily_chance_of_rain>0?`${forecast.day.daily_chance_of_rain}%`:""}</Typography>
                    </div>
                    <div>                      
                      <img src={forecast.day.condition.icon}></img>
                    </div>
                    <div>
                      <Typography>{props.unit==="fahrenheit"?forecast.day.maxtemp_f.toFixed(0):forecast.day.maxtemp_c.toFixed(0)}°</Typography>
                      <Typography>{props.unit==="fahrenheit"?forecast.day.mintemp_f.toFixed(0):forecast.day.mintemp_c.toFixed(0)}°</Typography>
                    </div>
                  </Stack>
                </AccordionSummary>
                <AccordionDetails>
                <div style={{overflowX:'scroll'}}>
                    <Stack direction={"row"} spacing={1}>
                      {forecast.hour.map((forecastHour:any, index: number)=>{
                        return(
                          <Stack key={`forecastHour_${index}`} direction="column" spacing={1}>
                            <Typography variant="caption">
                              {props.unit==="fahrenheit"?`${forecastHour.temp_f.toFixed(0)}°`:`${forecastHour.temp_c.toFixed(0)}°`}
                            </Typography>
                            <div><img src={forecastHour.condition.icon} width={48}></img></div>
                            <Typography variant="caption">
                              {forecastHour.time.split(" ")[1]}
                            </Typography>
                          </Stack>
                        )
                      })}
                    </Stack>
                  </div>
                  <TableContainer>
                    <Table>
                      <TableBody>
                        <TableRow>
                          <TableCell>Humidity</TableCell>
                          <TableCell>{`${forecast.day.avghumidity} %`}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Wind</TableCell>
                          <TableCell>{`${forecast.day.maxwind_mph} MPH`}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Sunrise</TableCell>
                          <TableCell>{forecast.astro.sunrise}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Sunset</TableCell>
                          <TableCell>{forecast.astro.sunset}</TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </TableContainer>
                  
                </AccordionDetails>
              </Accordion>
            )
          })}
        </Stack>
      </div>
      :
      <div>No data</div>
    }
    </div>
  );
}