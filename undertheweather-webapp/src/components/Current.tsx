import { Stack, TableContainer, Typography, Paper, Table, TableBody, TableRow, TableCell } from "@mui/material";
import { parse as parseDate, format as formatDate } from "date-fns";
import React, { useEffect } from "react";

interface ICurrentProps{
	unit: 'celcius'|'fahrenheit';
	data: any
}

export default function Current(props: ICurrentProps){
	const data = props.data;
	let currentEpoch:any, filteredHourForecast;
	if(data!==undefined){
		currentEpoch = data.location.localtime_epoch;
		filteredHourForecast = data.forecast.forecastday[0].hour.filter((item:any)=>{
			return item.time_epoch>currentEpoch?true:false;
		})
	}
	return(
		<div>
			{data!==undefined?
			<div>
				<div>
					<Typography textAlign={"left"} variant="h6">{data.location.name}</Typography>
					<Typography textAlign={"left"}>{data.location.region}, {data.location.country}</Typography>
				</div>
				<Stack direction="row" justifyContent="space-between" marginTop={"2em"}>
					<div>
						<Typography textAlign={"left"}>{ formatDate(parseDate(data.location.localtime,'yyyy-MM-dd HH:mm',new Date()), 'EEEE, MMMM Mo, HH:mm a')}
						</Typography>
						<Typography fontSize={"72px"} textAlign={"left"} color="primary">
							{props.unit==="fahrenheit"?`${data.current.temp_f.toFixed(0)}°`:`${data.current.temp_c.toFixed(0)}°`}
						</Typography>
						<Typography textAlign={"left"}>
							Feels like {props.unit==="fahrenheit"?`${data.current.feelslike_f.toFixed(0)}°`:`${data.current.feelslike_c.toFixed(0)}°`}
						</Typography>
					</div>
					<div>
						<div><img src={data.current.condition.icon} width={84}></img></div>
						<Typography>{data.current.condition.text}</Typography>
					</div>
				</Stack>
				<div>
					<Typography textAlign={"left"}>
						Chance of rain: {data.forecast.forecastday[0].day.daily_chance_of_rain}%
					</Typography>
				</div>
				<br/>
				<div style={{overflowX:'scroll'}}>
					<Stack direction={"row"} spacing={1}>
						{filteredHourForecast.map((forecast:any, index: number)=>{
							return(
								<Stack key={`forecast_${index}`} direction="column" spacing={1}>
									<Typography variant="caption">
										{props.unit==="fahrenheit"?`${forecast.temp_f.toFixed(0)}°`:`${forecast.temp_c.toFixed(0)}°`}
									</Typography>
									<div><img src={forecast.condition.icon} width={48}></img></div>
									<Typography variant="caption">
										{forecast.time.split(" ")[1]}
									</Typography>
								</Stack>
							)
						})}
					</Stack>
				</div>
				<br/>
				<div>
					<TableContainer>
						<Table>
							<TableBody>
								<TableRow>
									<TableCell>Humidity</TableCell>
									<TableCell>{`${data.current.humidity} %`}</TableCell>
								</TableRow>
								<TableRow>
									<TableCell>Wind</TableCell>
									<TableCell>{`${data.current.wind_mph} MPH, direction: ${data.current.wind_dir}`}</TableCell>
								</TableRow>
								<TableRow>
									<TableCell>Sunrise</TableCell>
									<TableCell>{data.forecast.forecastday[0].astro.sunrise}</TableCell>
								</TableRow>
								<TableRow>
									<TableCell>Sunset</TableCell>
									<TableCell>{data.forecast.forecastday[0].astro.sunset}</TableCell>
								</TableRow>
							</TableBody>
						</Table>
					</TableContainer>
				</div>
			</div>
			:
			<div>
				No data
			</div>
		}
		</div>
	)
}