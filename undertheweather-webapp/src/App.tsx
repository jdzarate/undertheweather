import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { AppBar, Box, Container, Tab, ToggleButton, ToggleButtonGroup, Toolbar, Typography, Button } from '@mui/material';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import { getTemperatureUnit, saveTemperatureUnit} from './utils';
import Current from './components/Current';
import Forecast from './components/Forecast';
import Progress from './components/Progress';

function App() {
  const [tabValue, setTabValue] = useState("1");
  const [temperatureUnit, setTemperatureUnit] = useState<'celcius'|'fahrenheit'>(getTemperatureUnit()||'fahrenheit');
  const [weatherData, setWeatherData] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [locationStatus, setLocationStatus] = useState<'yes'|'no'>();
  const [loadingText, setLoadingText] = useState("");

  const handleTabChange = (event: React.SyntheticEvent, newValue: string)=>{
    setTabValue(newValue);
  }
  useEffect(()=>{
    getLocation();
  },[]);

  const getLocation = ()=>{
    if('geolocation' in navigator){
      setIsLoading(true);
      setLoadingText("Attempting to get browser's location");
      setIsLoading(false);
      navigator.geolocation.getCurrentPosition((position)=>{
        setIsLoading(false);
        setLocationStatus('yes');
        console.log(`${position.coords.latitude},${position.coords.longitude}`)
        if(position!==undefined){
          getWeather(`${position.coords.latitude},${position.coords.longitude}`);
        }        
      }, ()=>{
        setLocationStatus('no');
        setIsLoading(false);
      })
    }
    else{
      setLocationStatus('no');
      console.log("geolocation not available.")
    }
  }

  const getWeather = (query: string)=>{
    const baseUrl = process.env.REACT_APP_API_URL;
    if(baseUrl!==undefined){
      setIsLoading(true);
      setLoadingText("Getting data from Service");
      fetch(`${baseUrl}/GetForecast?location=${query}`, {
        method:'GET',
        headers:{
          'Content-Type':'application/json'
        }
      })
      .then(response=>response.json())
      .then(data=>{
        setWeatherData(data);
        setIsLoading(false);
      }).catch((error)=>{
        setIsLoading(false);
        console.log("request error");
      })      
    } 
  }

  const handleChangeTemperatureUnit = (
    event: React.MouseEvent<HTMLElement>,
    newUnit: 'celcius'|'fahrenheit'
  )=>{
    setTemperatureUnit(newUnit);
    saveTemperatureUnit(newUnit);
  }
  return (
    <div className="App">
      <AppBar position='static'>
        <Toolbar>
          <Typography variant="h6" component="div" textAlign={"left"} sx={{ flexGrow: 1 }}>
            Under The Weather
          </Typography>
          <ToggleButtonGroup
            value={temperatureUnit}
            exclusive
            onChange={handleChangeTemperatureUnit}
            sx={{
              background:'#fff'
            }}
          >
            <ToggleButton value="fahrenheit">
            °F
            </ToggleButton>
            <ToggleButton value="celcius">
            °C
            </ToggleButton>
          </ToggleButtonGroup>
        </Toolbar>
      </AppBar>
      <Box sx={{ width: '100%', typography: 'body1' }}>
        <Container maxWidth="md" sx={{padding:0}}>
          {!isLoading?
            locationStatus==='yes'?
            <>
              <TabContext value={tabValue}>
              <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <TabList onChange={handleTabChange} centered>
                  <Tab label="Today" value="1"></Tab>
                  <Tab label="5 days" value="2"></Tab>
                </TabList>
              </Box>          
              <TabPanel value="1" className='tab-panel'>
                <Current unit={temperatureUnit} data={weatherData}/>
              </TabPanel>
              <TabPanel value="2" className='tab-panel'>
                <Forecast unit={temperatureUnit} data={weatherData}/>
              </TabPanel>
              </TabContext>
            </>:
            locationStatus==='no'?
              <div style={{paddingTop:'2em'}}>
                <Typography>
                  This app requires you to allow location permission in your browser. After you allow it, please refresh.
                </Typography>
              </div>
              :
              <div style={{paddingTop:'2em'}}>
                <Progress text="Attempting to get browser's location"/>
              </div>            
            :            
            <Progress text={loadingText}/>
        }
        </Container>
      </Box>
    </div>
  );
}

export default App;
