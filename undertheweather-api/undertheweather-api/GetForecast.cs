﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace undertheweather_api
{
    public static class GetForecast
    {
        [FunctionName("GetForecast")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            const int DAYS = 5;
            const string API_URL = "https://api.weatherapi.com/v1/forecast.json";
            string query = req?.Query["location"];

            string urlParameters = $"?key={Environment.GetEnvironmentVariable("WeatherApiKey")}&q={query}&days={DAYS}&aqi=no&alerts=no";
            log.LogInformation(urlParameters);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(API_URL);
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;

            if (response.IsSuccessStatusCode)
            {
                string responseDataString = await response.Content.ReadAsStringAsync();
                JObject json = JObject.Parse(responseDataString);
                return new OkObjectResult(json);
            }
            else
            {
                log.LogError(response.StatusCode.ToString());
                return new BadRequestResult();
            }
        }
    }
}

