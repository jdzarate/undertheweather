# Under The Weather

Under The Weather is a simple Weather Web Application that delivers forecast weather information up to 5 days. The app requires the user to allow location permission on the browser as it currently doesn't let you pick a custom location.

The app consists of two components:

* A Web App built with ReactJS and Typescript.
* An Azure Function App built with DotNet Core (C#) that proxies WeatherAPI ([weatherapi.com](https://weatherapi.com/)) calls. 

You can see a demo here: [https://undertheweatherapp.netlify.app/](https://undertheweatherapp.netlify.app/)

